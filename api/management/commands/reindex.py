from django.core.management.base import BaseCommand, CommandError
from api.models import Content


class Command(BaseCommand):
    help = 'This command will index all the content in database'

    def handle(self, *args, **options):
        self.stdout.write(self.style.WARNING('Initiating reindexing...'))

        articles = 0
        images = 0
        
        for content in Content.objects.all():
            self.stdout.write('Indexing %s' % content)
            content.index()

            if content.type == Content.ARTICLE:
                articles += 1
            
            elif content.type == Content.IMAGE:
                images += 1


        self.stdout.write(self.style.SUCCESS('Successfully indexed %s articles, %s images.' % (articles, images)))
