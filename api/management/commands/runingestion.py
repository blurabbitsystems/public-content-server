from django.core.management.base import BaseCommand, CommandError
from api.models import Aggregator


class Command(BaseCommand):
    help = 'This command will ingest data from aggregators'

    def add_arguments(self, parser):
        parser.add_argument(
            'aggregators',
            type=str,
            nargs='?',
            default='',
            help='Comma separated list of aggregator guids',
        )

    def handle(self, *args, **options):
        self.stdout.write(self.style.WARNING('Initiating ingestion...'))

        if options['aggregators']:
            query_set = Aggregator.objects.filter(guid__in=options['aggregators'].split(','))
        else:
            query_set = Aggregator.objects.all()

        for aggregator in query_set:
            self.stdout.write('Ingesting %s' % aggregator)
            aggregator.aggregate()

        self.stdout.write(self.style.SUCCESS('Aggregation completed successfully.'))
