from rest_framework import serializers
from api.models import Content
from api.serializers.publisher import PublisherSerializer


class ArticleSerializer(serializers.ModelSerializer):
    guid = serializers.UUIDField(format='hex')
    description = serializers.CharField(source='text_body')
    author = serializers.CharField(source='creator')
    publisher = PublisherSerializer(read_only=True)
    publisher_guid = serializers.UUIDField(source='publisher.guid', format='hex')
    original_url = serializers.CharField(source='web_url')

    class Meta:
        model = Content
        fields = ['guid', 'type', 'title', 'original_url', 'description', 'publisher', 'publisher_guid', 'author', 'published_at']


class ImageSerializer(serializers.ModelSerializer):
    guid = serializers.UUIDField(format='hex')
    attribution = serializers.CharField(source='text_body')
    photographer = serializers.CharField(source='creator')
    publisher = PublisherSerializer(read_only=True)
    publisher_guid = serializers.UUIDField(source='publisher.guid', format='hex')
    caption = serializers.CharField(source='title')

    class Meta:
        model = Content
        fields = ['guid', 'type', 'caption', 'attribution', 'publisher', 'publisher_guid', 'photographer', 'published_at', 'blob_url', 'thumbnail_url']
