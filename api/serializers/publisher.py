from rest_framework import serializers
from api.models import Publisher


class PublisherSerializer(serializers.ModelSerializer):
    guid = serializers.UUIDField(format='hex')

    class Meta:
        model = Publisher
        fields = ['guid', 'title']
