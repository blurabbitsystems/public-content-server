# Generated by Django 2.2.5 on 2020-01-22 11:40

from django.db import migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aggregator',
            name='credentials',
            field=jsonfield.fields.JSONField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='content',
            name='metadata',
            field=jsonfield.fields.JSONField(blank=True, null=True),
        ),
    ]
