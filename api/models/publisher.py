from django.db import models

from .utils import generate_guid


class Publisher(models.Model):
    guid = models.UUIDField(unique=True, default=generate_guid, editable=False)
    title = models.CharField(max_length=127)

    def __str__(self):
        return self.title
