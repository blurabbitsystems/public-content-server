import uuid


def generate_guid(to_hex=False):
    guid = uuid.uuid4()
    if to_hex:
        guid = guid.hex

    return guid
