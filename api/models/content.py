from django.db import models
from jsonfield import JSONField

from .utils import generate_guid


class Content(models.Model):
    ARTICLE = 'article'
    IMAGE = 'image'

    guid = models.UUIDField(unique=True, default=generate_guid, editable=False)
    type = models.CharField(max_length=15, choices=[(ARTICLE, 'Article'), (IMAGE, 'Image')])
    title = models.CharField(max_length=255)
    public_id = models.CharField(max_length=255, unique=True)
    web_url = models.TextField(null=True)
    text_body = models.TextField()
    publisher = models.ForeignKey('Publisher', on_delete=models.CASCADE)
    aggregator = models.ForeignKey('Aggregator', on_delete=models.CASCADE)
    creator = models.CharField(max_length=255, blank=True, null=True)
    published_at = models.DateTimeField()
    metadata = JSONField(blank=True, null=True)

    def __str__(self):
        return '[{0.type}] {0.title}'.format(self)

    @property
    def blob_url(self):
        if self.type == self.IMAGE:
            return (self.metadata or {}).get('blob_url', '')
        
        raise Exception('{0.type} do not have blob_url.'.format(self))

    @property
    def thumbnail_url(self):
        if self.type == self.IMAGE:
            return (self.metadata or {}).get('thumbnail_url', '')
        
        raise Exception('{0.type} do not have thumbnail_url.'.format(self))

    def index(self):
        from search_index.public_content import PublicContent
        PublicContent().store(self)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self.index()

    def to_es_doc(self):
        if self.type == self.ARTICLE:
            from api.serializers.content import ArticleSerializer
            return ArticleSerializer(self).data

        if self.type == self.IMAGE:
            from api.serializers.content import ImageSerializer
            return ImageSerializer(self).data
