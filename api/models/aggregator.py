from django.db import models
from jsonfield import JSONField

from parsers import PARSERS
from extractors import EXTRACTORS

from .content import Content
from .publisher import Publisher
from .utils import generate_guid


class Aggregator(models.Model):
    API = 'api'
    FEED = 'feed'

    guid = models.UUIDField(unique=True, default=generate_guid, editable=False)
    type = models.CharField(max_length=15, choices=[(API, 'API'), (FEED, 'Feed')])
    title = models.CharField(max_length=127)
    url = models.CharField(max_length=255)
    parser_name = models.CharField(max_length=16)
    extractor_name = models.CharField(max_length=16, default=None, null=True, blank=True)
    credentials = JSONField(blank=True, null=True)

    def __str__(self):
        return '[{0.type}] {0.title}'.format(self)

    def aggregate(self):
        parser = PARSERS[self.parser_name](self)
        for parsed_content in parser.get_parsed_content():
            fields = parsed_content.to_dict()
            public_id = fields.pop('public_id')

            try:
                content = Content.objects.get(public_id=public_id)
            except Content.DoesNotExist:
                content = Content(public_id=public_id)

            fields['publisher'], _ = Publisher.objects.get_or_create(title=fields.pop('publisher'))
            fields['aggregator'] = self

            for key, value in fields.items():
                setattr(content, key, value)

            if self.extractor_name and content.web_url:
                try:
                    extracted_content = EXTRACTORS[self.extractor_name](content.web_url)
                    content.title = extracted_content.title
                    content.text_body = extracted_content.description
                    content.creator = extracted_content.attribution
                except Exception:
                    print(f'[{self.title}] Failed to extract: [{self.extractor_name}]({content.web_url})')

            try:
                content.save()
            except Exception as e:
                print(f'[{self.title}] Error occurred when saving content [{public_id}]({content.web_url}): {e}')
