from django.urls import path

from .views import ContentView, SingleContentView, PublisherView

app_name = 'api'


urlpatterns = [
    path('content/<type>s/', ContentView.as_view()),
    path('content/<type>s/<str:guid>', SingleContentView.as_view()),

    path('publishers/', PublisherView.as_view()),
]
