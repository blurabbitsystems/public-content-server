from rest_framework.generics import ListAPIView

from api.models import Publisher
from api.serializers.content import PublisherSerializer


class PublisherView(ListAPIView):
    queryset = Publisher.objects.all()
    serializer_class = PublisherSerializer
