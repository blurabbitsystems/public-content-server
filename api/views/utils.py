def parse_article_query_params(params):
    parsed_params = {}

    for key, value in params.items():
        value = value if not isinstance(value, list) else value[0]
        if not value:
            continue

        if key == 'q':
            parsed_params['description__full'] = value

        elif key in ['author', 'title']:
            parsed_params['%s__full' % key] = value

        elif key == 'publishers':
            parsed_params['publisher_guid__in'] = value.split(',')

        elif key == 'guid':
            parsed_params['guid__eq'] = value

        elif key in ['offset', 'page_size']:
            parsed_params[key] = value

    return parsed_params


def parse_image_query_params(params):
    parsed_params = {}

    for key, value in params.items():
        value = value if not isinstance(value, list) else value[0]
        if not value:
            continue

        if key == 'q':
            parsed_params['attribution__full'] = value

        elif key in ['photographer', 'caption']:
            parsed_params['%s__full' % key] = value

        elif key == 'publishers':
            parsed_params['publisher_guid__in'] = value.split(',')

        elif key == 'guid':
            parsed_params['guid__eq'] = value

        elif key in ['offset', 'page_size']:
            parsed_params[key] = value

    return parsed_params
