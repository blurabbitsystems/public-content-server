from django.shortcuts import get_object_or_404
from rest_framework.generics import  RetrieveAPIView
from rest_framework.views import APIView
from rest_framework.response import Response

from api.models import Content
from api.serializers.content import ArticleSerializer, ImageSerializer
from search_index.public_content import PublicContent

from .utils import parse_article_query_params, parse_image_query_params

serializer_class_map = {
    'article': ArticleSerializer,
    'image': ImageSerializer,
}


class ContentView(APIView):

    def get_parsed_params(self):
        if self.kwargs['type'] == 'article':
            return parse_article_query_params(self.request.query_params)
        
        if self.kwargs['type'] == 'image':
            return parse_image_query_params(self.request.query_params)

    def get(self, request, **kwargs):
        search_params = self.get_parsed_params()
        index = PublicContent()
        result = index.search(type__eq=kwargs['type'], **search_params)

        return Response(result)


class SingleContentView(RetrieveAPIView):
    lookup_field = 'guid'

    def get_serializer_class(self):
        return serializer_class_map[self.kwargs['type']]
    
    def get_queryset(self):
        return Content.objects.filter(type=self.kwargs['type'])
