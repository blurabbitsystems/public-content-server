from django.contrib import admin

from .models import Content, Aggregator, Publisher


@admin.register(Aggregator)
class AggregatorAdmin(admin.ModelAdmin):
    list_display = ['guid', 'title', 'type', 'url', 'parser_name']
    list_display_links = ['guid']
    search_fields = ['guid', 'title', 'url']


@admin.register(Publisher)
class PublisherAdmin(admin.ModelAdmin):
    list_display = ['guid', 'title']
    list_display_links = ['guid']
    search_fields = ['guid', 'title']


@admin.register(Content)
class ContentAdmin(admin.ModelAdmin):
    list_display = ['guid', 'title', 'type', 'publisher', 'published_at']
    list_display_links = ['guid']
    search_fields = ['guid', 'title']
