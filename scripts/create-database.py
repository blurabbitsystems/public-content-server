import pymysql

from httpserver.settings import DATABASES

def run():
    db = DATABASES['default']
    conn = pymysql.connect(host=db['HOST'], user=db['USER'], password=db['PASSWORD'])

    try:
        conn.cursor().execute('CREATE DATABASE `%s` CHARACTER SET utf8 COLLATE utf8_general_ci' % db['NAME'])
        print('Created database %s' % db['NAME'])
    except Exception:
        pass
    finally:
        conn.close()

if __name__ == '__main__':
    run()
