from httpserver.settings import ES_CONFIG
from search_index import create_index, get_es_client

def run():
    index = ES_CONFIG['INDEX']
    client = get_es_client()
    create_index(client, index)

if __name__ == '__main__':
    run()
