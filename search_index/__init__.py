from httpserver.settings import ES_CONFIG
from elasticsearch import Elasticsearch, exceptions


def get_es_client():
    return Elasticsearch([{'host': ES_CONFIG['HOST'], 'port': ES_CONFIG['PORT']}])


def create_index(client, index_name):
    try:
        client.indices.create(index_name)
    except exceptions.RequestError:
        pass


def delete_index(client, index_name):
    try:
        client.indices.delete(index_name)
    except exceptions.NotFoundError:
        pass
