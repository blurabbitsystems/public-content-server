from django.test import TestCase

from ..utils import ESQueryBuilder, InvalidOperator


class TestESQueryBuilder(TestCase):
    
    def test_get_es_clauses(self):
        query_clauses, filter_clauses = ESQueryBuilder._get_es_clauses(name='anthony', age__full='56', weight__gt='200')
        assert query_clauses == [
            {'match': {'name': 'anthony'}},
            {'match_phrase': {'age': '56'}},
        ]
        assert filter_clauses == [
            {'range': {'weight': {'gt': '200'}}},
        ]

    def test_get_es_clauses__invalid_operator(self):
        with self.assertRaises(InvalidOperator):
            ESQueryBuilder._get_es_clauses(name__hghd='test')

    def test_build_query(self):
        query = ESQueryBuilder.build_query(operator='OR', source__eq='risingbd', name__in=['anthony', 'hopkins'], title='omg lmao', weight__gte='200')
        assert query == {
            'query': {
                'bool': {
                    'should': [
                        {'match': {'title': 'omg lmao'}}
                    ],
                    'filter': [
                        {'term': {'source': 'risingbd'}},
                        {'terms': {'name': ['anthony', 'hopkins']}},
                        {'range': {'weight': {'gte': '200'}}}
                    ]
                }
            }
        }
