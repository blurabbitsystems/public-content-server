from django.conf import settings

from api.models import Content

from . import get_es_client, create_index, delete_index
from .utils import ESQueryBuilder


class PublicContent(object):
    def __init__(self):
        self.index_name = settings.ES_CONFIG['INDEX']
        self.client = get_es_client()

    def store(self, db_obj):
        doc = db_obj.to_es_doc()
        self.client.index(index=self.index_name, id=doc['guid'], body=doc)

    def search(self, operator=None, **params):
        # @operator - one of ['AND', 'OR', 'NOT'], default is 'AND'
        # @params - each param should have the following format:
        #   attribute__operator = value
        #   attribute is a key in an es-doc
        #   operator is applied on that attribute
        #   value is the expected value to match
        #
        # Example 1: index.search(operator='OR', name__full='Michael Scofield', age__gte=30)
        #   This will return docs that has: ('Michael Scofield' in doc.name or doc.age >= 30)
        #
        # Example 2: index.search(name='Michael Scofield', age__eq=20)
        #   This will return docs that has: (('Michael' in doc.name or 'Scofield' in doc.name) and doc.age == 20)

        es_query = {'from': params.pop('offset', 0), 'size': params.pop('page_size', 20)}
        es_query.update(ESQueryBuilder.build_query(operator=operator, **params))
        es_query['sort'] = [{'published_at': 'desc'}, '_score']
        resp = self.client.search(index=self.index_name, body=es_query)

        results = []
        for hit in resp['hits']['hits']:
            results.append(hit['_source'])

        total = resp['hits']['total']['value']
        returned = len(results)

        return {'total': total, 'returned': returned, 'results': results}

    def remove(self, guid):
        self.client.delete(index=self.index_name, id=guid)

    def drop(self):
        delete_index(self.client, self.index_name)
