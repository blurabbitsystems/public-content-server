class ESQueryBuilder(object):
    operator_map = {
        'AND': 'must',
        'OR': 'should',
        'NOT': 'must_not',
    }

    query_map = {
        '': 'match',
        'full': 'match_phrase',
    }

    filter_map = {
        'gt': 'range',
        'lt': 'range',
        'gte': 'range',
        'lte': 'range',
        'in': 'terms',
        'eq': 'term',
    }

    @classmethod
    def build_query(cls, operator=None, **kwargs):
        query_clauses, filter_clauses = cls._get_es_clauses(**kwargs)

        operator = operator or 'AND'
        es_operator = cls.operator_map[operator]

        bool_query = {es_operator: query_clauses, 'filter': filter_clauses}
        query = {'query': {'bool': bool_query}}

        return query

    @classmethod
    def _get_es_clauses(cls, **kwargs):
        query_clauses = []
        filter_clauses = []

        for key, value in kwargs.items():
            name, operator = key.split('__') if '__' in key else (key, '')

            if operator in cls.query_map:
                es_operator = cls.query_map[operator]
                query_clauses.append({es_operator: {name: value}})

            elif operator in cls.filter_map:
                es_operator = cls.filter_map[operator]
                if es_operator == 'range':
                    value = {operator: value}

                filter_clauses.append({es_operator: {name: value}})

            else:
                raise InvalidOperator('Supported operators: {} '.format(list(cls.query_map.keys()) + list(cls.filter_map.keys())))

        return query_clauses, filter_clauses


class InvalidOperator(Exception):
    pass
