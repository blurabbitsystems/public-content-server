from datetime import datetime
from dateutil.tz import tzoffset
from unittest.mock import Mock
from unittest.mock import patch

from parsers.rss import RSSParsedContent, RSSParser

from api.models import Aggregator, Content
from tests.utils import make_aggregator, make_content, ESTestCase


class TestAggregator(ESTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.aggregator = make_aggregator(parser_name='rss')

    @patch.object(RSSParser, 'get_parsed_content')
    def test_aggregate(self, m_get_parsed_content):
        dummy_article = {
            'publisher': 'test-publisher',
            'public_id': 'test-id',
            'title': 'dummy-title',
            'type': 'article',
            'text_body': '<p>dummybody</p>',
            'creator': 'dummy-author',
            'published_at': datetime(2020, 1, 23, 20, 48, 37, tzinfo=tzoffset(None, 21600)),

        }
        m_get_parsed_content.return_value = [
            Mock(to_dict=Mock(return_value=dummy_article)),
        ]
        self.aggregator.aggregate()
        
        content = Content.objects.get(public_id='test-id')
        assert content.publisher.title == 'test-publisher'
        assert content.aggregator.id == self.aggregator.id
        for field in ['title', 'type', 'text_body', 'creator', 'published_at']:
            assert getattr(content, field) == dummy_article[field]

    @patch.object(RSSParser, 'get_parsed_content')
    def test_aggregate__overwrite(self, m_get_parsed_content):
        content = make_content()
        dummy_article = {
            'publisher': 'test-publisher',
            'public_id': content.public_id,
            'title': 'dummy-title',
        }
        m_get_parsed_content.return_value = [
            Mock(to_dict=Mock(return_value=dummy_article)),
        ]
        self.aggregator.aggregate()
        
        content = Content.objects.get(public_id=content.public_id)
        assert content.title == 'dummy-title'
