import httpretty

from django.test import TestCase
from extractors.buzz_feed import BuzzFeedExtractor
from ..utils import read_sample


class TestBuzzFeedExtractor(TestCase):

    @classmethod
    @httpretty.activate
    def setUpTestData(cls):
        url = 'http://buzzfeed.html'
        httpretty.register_uri(httpretty.GET, url, body=read_sample('buzz_feed.html'))
        cls.content = BuzzFeedExtractor(url)
    
    def test_title(self):
        assert self.content.title == 'These Before And After Photos Show The Damage In Beirut After The Massive Explosion'

    def test_description(self):
        assert self.content.description == '''<p>Dozens of people were killed and thousands more injured after a gigantic explosion tore through Beirut on Tuesday, pulverizing entire neighborhoods and devastating a nation facing political and economic crises, as well as the coronavirus pandemic.</p><p>Lebanese authorities have said they suspect the disaster was caused by almost 3,000 tons of ammonium nitrate being stored unsafely at a warehouse at a port in the capital city.</p><p>The force of the explosion was estimated by scientists at the University of Sheffield to be one-tenth the power of the atomic bomb dropped on Hiroshima in 1945 during World War II. It was "unquestionably one of the biggest non-nuclear explosions in history," the researchers said.</p><p>Officials have said at least 135 people died and 4,000 others were injured.</p><p>These Maxar satellite photos, some of which capture sites in Beirut before and after the blast, provide a glimpse at the monumental scale of the damage.</p><p>Here's the port area of Beirut on June 9, 2020, and again on Aug. 5 following the explosion:</p><p>Here's the port area of Beirut on June 9, 2020, and again on Aug. 5 following the explosion:</p><p>This is a close-up of the grain silo at the Beirut port on June 9, 2020, and then on Aug. 5:</p><p>This is a close-up of the grain silo at the Beirut port on June 9, 2020, and then on Aug. 5:</p><p>The Orient Queen cruise ship is seen docked at the Beirut port on July 31, then on Aug. 5 where it had capsized after the explosion:</p><p>The Orient Queen cruise ship is seen docked at the Beirut port on July 31, then on Aug. 5 where it had capsized after the explosion:</p><p>Here's another view of the grain solo obliterated by the explosion:</p><p><img src=https://img.buzzfeed.com/buzzfeed-static/static/2020-08/5/21/asset/5d2a92c23054/sub-buzz-559-1596661960-7.jpg></p><p>And these are damaged buildings and warehouses in Beirut after the blast:</p><p><img src=https://img.buzzfeed.com/buzzfeed-static/static/2020-08/5/21/asset/a40927c447a5/sub-buzz-563-1596662052-12.jpg></p>'''

    def test_attribution(self):
        assert self.content.attribution == 'David Mack'

    def test_images(self):
        assert self.content.images == [
            'https://img.buzzfeed.com/buzzfeed-static/static/2020-08/5/21/asset/5d2a92c23054/sub-buzz-559-1596661960-7.jpg',
            'https://img.buzzfeed.com/buzzfeed-static/static/2020-08/5/21/asset/a40927c447a5/sub-buzz-563-1596662052-12.jpg',
        ]
