import httpretty

from django.test import TestCase
from extractors.the_daily_star import TheDailyStarExtractor
from ..utils import read_sample


class TestTheDailyStarExtractor(TestCase):

    @classmethod
    @httpretty.activate
    def setUpTestData(cls):
        url = 'http://dailystartestdomain.html'
        httpretty.register_uri(httpretty.GET, url, body=read_sample('the_daily_star.html'))
        cls.content = TheDailyStarExtractor(url)
    
    def test_title(self):
        assert self.content.title == 'Alibaba\'s Lazada appoints Chun Li as new CEO'

    def test_description(self):
        assert self.content.description == '''<p>Lazada, the Southeast Asian arm of Chinese e-commerce firm Alibaba Group Holding, said on Friday it would replace its chief executive officer, as it battles for dominance in the fast-growing market.</p><p>Lazada Group Chief Executive Officer and co-founder Pierre Poignant will be replaced by Chun Li, a former Alibaba executive, who is currently Lazada co-president and head of its Indonesia operations, the company said in a statement.</p><p>Reuters reported the move earlier on Friday, citing sources. Two people with knowledge of the matter said the reshuffle is due to the e-commerce firm's middling performance.</p><p>Lazada has struggled to fend off rivals like the Singapore-headquartered SEA's e-commerce arm Shopee, which is backed by Alibaba rival Tencent, for control of the fast-growing market of 650 million consumers.</p><p>The firm said Li would work to improve "Lazada's competitive advantage through data technology application and business localisation", and Lazada had seen "healthy growth" under Poignant.</p><p>Lazada said it had over 70 million users in the year to the end of March.</p><p>Poignant had himself replaced Lucy Peng, an Alibaba co-founder, who stepped down as CEO after nine months in 2018 although she remains executive chairwoman. Lazada said Poignant will now become special assistant to Alibaba Group Chairman and CEO Daniel Zheng.</p><p>Alibaba has long had struggles with managing Lazada, a company it owns 90% of after investing $3 billion since 2016, with employees highlighting a long-running culture clash with management from China.</p><p>According to VentureCap Insights, Lazada received a $1.2 billion injunction from Alibaba this year. It was not clear what the injunction related to and Lazada did not immediately respond to requests for comment.</p><p>Three sources told Reuters Lazada is also examining whether to rebrand or shut down LazMall, its take on Alibaba's Tmall marketplace.</p><p>James Sullivan, who headed Lazada Logistics, would also step down, two people said. Sullivan was not immediately reachable for comment, but his LinkedIn profile indicated he left Lazada in April.</p>'''

    def test_attribution(self):
        assert self.content.attribution == 'Reuters, Singapore'

    def test_images(self):
        assert self.content.images == ['https://assetsds.cdnedge.bluemix.net/sites/default/files/styles/big_2/public/feature/images/alibaba_15.jpg?itok=pta-_Oq6']
