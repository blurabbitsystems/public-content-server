import httpretty

from django.test import TestCase
from extractors.rising_bd import RisingBDExtractor
from ..utils import read_sample


class TestRisingBDExtractor(TestCase):

    @classmethod
    @httpretty.activate
    def setUpTestData(cls):
        url = 'http://risingbdtestdomain.html'
        httpretty.register_uri(httpretty.GET, url, body=read_sample('rising_bd.html'))
        cls.content = RisingBDExtractor(url)
    
    def test_title(self):
        assert self.content.title == '‘তারা আমার জীবন ধ্বংস করে দিচ্ছে’'

    def test_description(self):
        assert self.content.description == '<p><img src=https://cdn.risingbd.com/media/imgAll/2020July/sooraj-pancholi-risingbd-2008061141.jpg></p><p>বলিউড অভিনেতা সুরজ পাঞ্চোলি। অভিনেত্রী জিয়া খানের আত্মহত্যার ঘটনায় আলোচনার কেন্দ্রবিন্দুতে ছিলেন তিনি। এবার অভিনেতা সুশান্ত সিং রাজপুত ও দিশা সালিয়ানের আত্মহত্যার ঘটনায় তার নাম জড়িয়েছে। এতে ভীষণ চটেছেন ‘হিরো’ সিনেমাখ্যাত এই অভিনেতা।</p><p>এক সাক্ষাৎকারে সুরজ পাঞ্চোলি বলেন, ‘আমার প্রথম সিনেমা পেতে সবরকম চেষ্টায় করেছি। তাই, এটা আমার প্যাসন, খুব সহজে হার মানব না। আর যে সকল মানুষ আমাকে নিয়ে কথা বলছে তাদের ন্যূনতম বুদ্ধি ও মানবতা থাকা প্রয়োজন। কারণ এটা ঠিক নয়। তারা আমার জীবন ধ্বংস করে দিচ্ছে। জানি না সুশান্ত আত্মহত্যা করেছেন কি না। আমি জানি না। কিন্তু তারা আমাকে আত্মহত্যার পথে ঠেলে দিচ্ছে। আমার কাছে এটিই মনে হচ্ছে।’</p><p>এই অভিনেতা আরো বলেন, ‘এখন ইতিবাচক থাকার চেষ্টা করছি। পরিবারের সঙ্গে বিষয়টি নিয়ে আলোচনা করছি না কারণ তারা ইতোমধ্যে আমাকে নিয়ে অনেক চাপে আছেন। সবসময় আমাকে নিয়ে দুশ্চিন্তা করছেন। মা ধারণা করছেন, আমি খারাপ কিছু করে ফেলব। আমার সঙ্গে এ বিষয়ে কয়েকবার কথাও বলেছেন।’</p><p>এর আগে গুঞ্জন ওঠে, সুশান্তের মৃত্যুর আগের রাতে পার্টি করেছেন সুরজ। এছাড়া দিশা সালিয়ানের সঙ্গে এই অভিনেতার বন্ধুত্ব ছিল। যদিও সুরজ ও তার পরিবার বরাবরই এই গুঞ্জনগুলো উড়িয়ে দিয়ে আসছেন।</p>'

    def test_attribution(self):
        assert self.content.attribution == 'ঢাকা/মারুফ'

    def test_images(self):
        assert self.content.images == ['https://cdn.risingbd.com/media/imgAll/2020July/sooraj-pancholi-risingbd-2008061141.jpg']
