import os

from django.conf import settings
from django.utils import timezone
from django.test import TestCase

from api.models import Content, Aggregator, Publisher
from api.models.utils import generate_guid
from search_index import get_es_client, create_index, delete_index


def read_sample(filename):
    dirpath = os.path.dirname(os.path.realpath(__file__))
    with open('%s/__samples__/%s' % (dirpath, filename), 'rb') as f:
        return f.read()


def make_aggregator(**kwargs):
    defaults = {
        'type': 'feed',
        'title': generate_guid(to_hex=True),
        'url': 'https//%s' % generate_guid(to_hex=True),
        'parser_name': generate_guid(to_hex=True)[:15],
    }
    defaults.update(kwargs)
    return Aggregator.objects.create(**defaults)


def make_publisher(**kwargs):
    defaults = {
        'title': generate_guid(to_hex=True),
    }
    defaults.update(kwargs)
    return Publisher.objects.create(**defaults)


def make_content(**kwargs):
    defaults = {
        'publisher_id': make_publisher().id,
        'aggregator_id': make_aggregator().id,
        'public_id': generate_guid(to_hex=True),
        'title': generate_guid(to_hex=True),
        'type': 'article',
        'text_body': '<p>%s</p>' % generate_guid(to_hex=True),
        'creator': generate_guid(to_hex=True),
        'published_at': timezone.now(),
    }
    defaults.update(kwargs)
    return Content.objects.create(**defaults)


def get_test_es_config():
    test_config = dict(**settings.ES_CONFIG)
    test_config['INDEX'] = 'test_' + test_config['INDEX']

    return test_config


class ESTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        cls._prod_es_config = settings.ES_CONFIG
        settings.ES_CONFIG = get_test_es_config()

        es_client = get_es_client()
        delete_index(es_client, settings.ES_CONFIG['INDEX'])
        create_index(es_client, settings.ES_CONFIG['INDEX'])

        super().setUpClass()
        es_client.indices.refresh(settings.ES_CONFIG['INDEX'])

    @classmethod
    def tearDownClass(cls):
        delete_index(get_es_client(), settings.ES_CONFIG['INDEX'])
        super().tearDownClass()

        settings.ES_CONFIG = cls._prod_es_config
