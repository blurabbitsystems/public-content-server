#!bin/sh

export DB_HOST=$(jq -r '.DB_HOST // "127.0.0.1"' .config/env.json)
export ES_HOST=$(jq -r '.ES_HOST // "127.0.0.1"' .config/env.json)
export ES_PORT=$(jq -r '.ES_PORT // "9200"' .config/env.json)

while ! nc -zw1 $DB_HOST 3306
do
  echo "Waiting for connection to rds-host: ${DB_HOST}"
  sleep 1
done

while ! nc -zw1 $ES_HOST $ES_PORT
do
  echo "Waiting for connection to es-host: ${ES_HOST}"
  sleep 1
done

python -m scripts.create-database
python -m scripts.create-es-index
python manage.py migrate
python manage.py runserver 0.0.0.0:8000
