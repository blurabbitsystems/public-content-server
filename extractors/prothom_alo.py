from .base import BaseExtractor


class ProthomAloExtractor(BaseExtractor):

    def get_content_div(self):
        return self.soup.find('div', {'class': 'detail_widget'})

    @property
    def images(self):
        images = []
        for img in self.div.find_all('img'):
            src = img.attrs.get('src')
            if src:
                images.append(src)

        return images

    @property
    def description(self):
        description = ''
        content_div = self.div.find('div', {'itemprop': 'articleBody'})
        for p_tag in content_div.find_all('p', {'class': None}):
            img = p_tag.find('img')

            if not img:
                text = p_tag.text.strip()
            else:
                src = img.attrs.get('src')
                text = f'<img src={src}>' if src else ''

            if text:
                description += f'<p>{text}</p>'

        return description

    @property
    def attribution(self):
        author_div = self.div.find('div', {'class': 'author'})
        span = author_div and author_div.find('span', {'class': 'name'})
        return span.text if span else 'Prothom Alo'

    @property
    def title(self):
        h1 = self.div.find('h1', {'class': 'title'})
        return h1.text.strip() if h1 else ''
