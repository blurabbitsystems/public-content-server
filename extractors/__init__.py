from .buzz_feed import BuzzFeedExtractor
from .prothom_alo import ProthomAloExtractor
from .rising_bd import RisingBDExtractor
from .the_daily_star import TheDailyStarExtractor

EXTRACTORS = {
    'buzz_feed': BuzzFeedExtractor,
    'prothom_alo': ProthomAloExtractor,
    'rising_bd': RisingBDExtractor,
    'the_daily_star': TheDailyStarExtractor,
}
