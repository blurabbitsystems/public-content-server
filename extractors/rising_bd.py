from .base import BaseExtractor


class RisingBDExtractor(BaseExtractor):

    def get_content_div(self):
        return self.soup.find('div', {'class': 'DDetailsContent'})

    @property
    def images(self):
        images = []
        for img in self.div.find_all('img'):
            src = img.attrs.get('src')
            if src:
                images.append(src)

        return images

    @property
    def description(self):
        description = ''
        for tag in self.div.find_all(['p', 'img']):
            if tag.name == 'p' and tag.attrs.get('class'):
                continue

            if tag.name == 'img':
                src = tag.attrs.get('src')
                text = f'<img src={src}>' if src else ''
            else:
                text = tag.text.strip()

            if text:
                description += f'<p>{text}</p>'

        return description

    @property
    def attribution(self):
        p_tag = self.div.find('p', {'class': 'WritterInitial'})
        text = p_tag.text.strip() if p_tag else ''
        return text

    @property
    def title(self):
        # import ipdb; ipdb.set_trace()
        h1 = self.div.find('h1')
        return h1.text.strip() if h1 else ''
