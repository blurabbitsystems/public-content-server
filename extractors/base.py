import requests

from bs4 import BeautifulSoup


class BaseExtractor(object):
    soup = None
    div = None

    def __init__(self, url):
        resp = requests.get(url)
        resp.raise_for_status()

        self.soup = BeautifulSoup(resp.content, 'lxml')
        self.div = self.get_content_div()

    def get_content_div(self):
        raise NotImplementedError

    @property
    def title(self):
        raise NotImplementedError

    @property
    def description(self):
        raise NotImplementedError

    @property
    def attribution(self):
        raise NotImplementedError

    @property
    def images(self):
        raise NotImplementedError

    def __str__(self):
        return 'Title: {0.title}\nAuthor: {0.attribution}\nDescription: {0.description}\nImages: {0.images}'.format(self)
