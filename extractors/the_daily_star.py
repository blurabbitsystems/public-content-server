from .base import BaseExtractor


class TheDailyStarExtractor(BaseExtractor):

    def get_content_div(self):
        return self.soup.find('div', {'class': 'detailed-page'})

    @property
    def images(self):
        images = []
        featured_image_div = self.div.find('div', {'class': 'featured-image'})
        if featured_image_div is None:
            return images

        for img in featured_image_div.find_all('img'):
            src = img.attrs.get('src')
            if src:
                images.append(src)

        return images

    @property
    def description(self):
        description = ''
        content_div = self.div.find('div', {'class': 'node-content'})
        for tag in content_div.find_all(['p', 'img']):
            if tag.name == 'p' and tag.attrs.get('class'):
                continue

            if tag.name == 'img':
                src = tag.attrs.get('src')
                text = f'<img src={src}>' if src else ''
            else:
                text = tag.text.strip()

            if text:
                description += f'<p>{text}</p>'

        return description

    @property
    def attribution(self):
        author_div = self.div.find('div', {'class': 'author-name'})
        span = author_div.find('span', {'itemprop': 'name'})
        return span.text.strip() if span.text else ''

    @property
    def title(self):
        h1 = self.div.find('h1', {'itemprop': 'headline'})
        return h1.text.strip() if h1 else ''
