import re

from .base import BaseExtractor


class BuzzFeedExtractor(BaseExtractor):

    def get_content_div(self):
        return self.soup.find('main', {'id': ['news-content', 'buzz-content']})

    @property
    def images(self):
        images = []
        for figure in self.div.find_all('figure'):
            img = figure.find('img')
            src = img and img.attrs.get('src')
            if src and src.startswith('https://img.buzzfeed.com/'):
                images.append(src)

        return images

    @property
    def description(self):
        description = ''
        for tag in self.div.find_all(['p', 'figure']):
            if tag.name == 'p' and tag.attrs.get('class'):
                continue

            if tag.name == 'figure':
                img = tag.find('img')
                src = img and img.attrs.get('src')
                if src and src.startswith('https://img.buzzfeed.com/'):
                    text = f'<img src={src}>'
            else:
                text = tag.text.strip()

            if text:
                description += f'<p>{text}</p>'

        return description

    @property
    def attribution(self):
        pattern = re.compile(r'bylineName__.*')
        p_tag = self.div.find('span', {'class': ['news-byline-full__name', pattern]})
        text = p_tag.text.strip() if p_tag else 'BuzzFeed News'
        return text

    @property
    def title(self):
        pattern = re.compile(r'title__.*')
        h1 = self.div.find('h1', {'class': ['news-article-header__title', pattern]})
        return h1.text.strip() if h1 else ''
