import re

from lxml import html
from datetime import datetime

from api.models.content import Content

from .base import ParsedContent, BaseParser, TextFormatter


class RSSTextFormatter(TextFormatter):
    @staticmethod
    def format_text(raw_text):
        dom = html.fromstring(raw_text)
        p_tags = dom.xpath('//p')
        p_tag_texts = []
        for p_tag in p_tags:
            text = p_tag.text_content().strip()
            text = re.sub(r'(\n+)', ' ', text)
            text = re.sub(r' +', ' ', text)
            if text:
                p_tag_texts.append('<p>%s</p>' % text)

        return ''.join(p_tag_texts)


class RSSParsedContent(ParsedContent):
    formatter_class = RSSTextFormatter

    @property
    def title(self):
        return self.item_dom.find('title').text

    @property
    def _raw_text_body(self):
        try:
            return self.item_dom.find('content:encoded', self.item_dom.nsmap).text
        except Exception:
            return self.item_dom.find('description').text

    @property
    def creator(self):
        return self.publisher

    @property
    def _raw_public_id(self):
        return self.item_dom.find('guid').text

    @property
    def web_url(self):
        return self.item_dom.find('link').text

    @property
    def publisher(self):
        channel_dom = self.item_dom.getparent()
        return channel_dom.find('title').text

    @property
    def _raw_published_at(self):
        return self.item_dom.find('pubDate').text

    @property
    def metadata(self):
        return {}

    @property
    def type(self):
        return Content.ARTICLE


class RSSParser(BaseParser):
    ITEM_XPATH = '//item'
    parsed_content_class = RSSParsedContent
