import requests
import re
import hashlib
import pytz

from httpserver.settings import TIME_ZONE
from dateutil.parser import parse as parse_datetime
from lxml import etree


class TextFormatter(object):
    @staticmethod
    def format_text(raw_text):
        p_tags = re.findall('<p>[^<].+?</p>', raw_text)
        return ''.join(p_tags)


class ParsedContent(object):
    formatter_class = TextFormatter

    def __init__(self, item_dom):
        self.item_dom = item_dom

    @property
    def title(self):
        raise NotImplementedError

    @property
    def _raw_text_body(self):
        raise NotImplementedError

    @property
    def text_body(self):
        return self.formatter_class.format_text(self._raw_text_body)

    @property
    def creator(self):
        raise NotImplementedError

    @property
    def _raw_public_id(self):
        raise NotImplementedError

    @property
    def public_id(self):
        return hashlib.sha256(self._raw_public_id.encode('utf-8')).hexdigest()

    @property
    def web_url(self):
        raise NotImplementedError

    @property
    def publisher(self):
        raise NotImplementedError

    @property
    def _raw_published_at(self):
        raise NotImplementedError

    @property
    def published_at(self):
        timestamp = parse_datetime(self._raw_published_at)
        return timestamp.astimezone(pytz.timezone(TIME_ZONE))

    @property
    def type(self):
        raise NotImplementedError

    @property
    def metadata(self):
        return {}

    def to_dict(self):
        fields = ['title', 'type', 'text_body', 'creator', 'public_id', 'web_url', 'publisher', 'published_at', 'metadata']
        return {field: getattr(self, field) for field in fields}


class BaseParser(object):
    ITEM_XPATH = NotImplemented
    parsed_content_class = ParsedContent

    def __init__(self, aggregator):
        self.aggregator = aggregator
    
    def _get_raw_data(self):
        resp = requests.get(self.aggregator.url)
        resp.raise_for_status()

        return etree.fromstring(resp.content)

    def get_parsed_content(self):
        dom = self._get_raw_data()
        for item in dom.xpath(self.ITEM_XPATH):
            yield self.parsed_content_class(item)
