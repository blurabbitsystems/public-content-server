from .rss import RSSParser

PARSERS = {
    'rss': RSSParser,
}
