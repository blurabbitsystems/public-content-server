from django.test import TestCase, Client

from tests.utils import make_content, ESTestCase
from api.models.utils import generate_guid


class TestContentEndpoints(ESTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.client = Client()
        cls.image_guid = generate_guid()
        cls.article_guid = generate_guid()

        make_content(type='image', guid=cls.image_guid)
        make_content(type='article', guid=cls.article_guid)
        make_content(type='image')
        make_content(type='article')

    def test_get_all_articles(self):
        resp = self.client.get('/api/content/articles/')
        assert resp.status_code == 200

        data = resp.json()
        articles = data['results']

        assert data['total'] == 2
        assert data['returned'] == 2
        
        assert len(articles) == 2
        assert list(articles[0].keys()) == ['guid', 'type', 'title', 'original_url', 'description', 'publisher', 'publisher_guid', 'author', 'published_at']

    def test_get_all_images(self):
        resp = self.client.get('/api/content/images/')
        assert resp.status_code == 200

        data = resp.json()
        images = data['results']

        assert data['total'] == 2
        assert data['returned'] == 2

        assert list(images[0].keys()) == ['guid', 'type', 'caption', 'attribution', 'publisher', 'publisher_guid', 'photographer', 'published_at', 'blob_url', 'thumbnail_url']

    def test_get_single_article(self):
        resp = self.client.get('/api/content/articles/{}'.format(self.article_guid))
        assert resp.status_code == 200

        article = resp.json()
        assert article['guid'] == self.article_guid.hex

    def test_get_single_image(self):
        resp = self.client.get('/api/content/images/{}'.format(self.image_guid))
        assert resp.status_code == 200

        image = resp.json()
        assert image['guid'] == self.image_guid.hex
